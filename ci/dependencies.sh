#!/usr/bin/env sh

set -eu

# Add python pip and bash
apk add --no-cache libsodium-dev libffi-dev musl-dev openssl-dev make gcc python3-dev py3-pip bash

# Install docker-compose via pip
pip3 install --no-cache-dir docker-compose
docker-compose -v
